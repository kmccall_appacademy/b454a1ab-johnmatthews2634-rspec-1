def add(num_one, num_two)
  num_one + num_two
end

def subtract(num_one, num_two)
  num_one - num_two
end

def sum(array_of_nums)
  if array_of_nums == []
    return 0
  else
    array_of_nums.reduce(:+)
  end
end

def multiply(array_of_nums)
  array_of_nums.reduce(:*)
end

def power(num, exponent)
  num ** exponent
end

def factorial(num)
  factorial_range = (1..num).to_a
  if num < 2
    return 1
  else
    factorial_range.reduce(:*)
  end
end
