def translate(string)
 words = string.split
 pig_latin_word_array = []
 words.each do |word|
   pig_latin_word_array << pig_latin_helper_metho(word)
 end
 pig_latin_word_array.join(" ")
end

def pig_latin_helper_metho(word)
  vowels = "aeiou"
  letters_array = word.chars
  new_array_for_word = []
  first_vowel_index = nil
  previous_letter = nil
  letters_array.each_with_index do |letter, idx|
    if vowels.include?(letter)
      if idx == 0
        letters_array << "a" << "y"
        break
      elsif letter == "u"
        if previous_letter == "q"
          next
        else
          first_vowel_index = idx
          break
        end
      else
        first_vowel_index = idx
        break
      end
    else
      previous_letter = letter
      next
    end
  end
  if first_vowel_index.nil?
    return letters_array.join
  else
    new_array_for_word << letters_array[first_vowel_index..-1]
    new_array_for_word << letters_array[0...first_vowel_index]
    new_array_for_word << "a" << "y"
  end
  new_array_for_word.join
end
