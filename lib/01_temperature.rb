def ftoc(temp_fahrenheit)
  (temp_fahrenheit - 32) * 5 / 9
end

def ctof(temp_celsius)
  temp_celsius * 9.0 / 5 + 32
end
