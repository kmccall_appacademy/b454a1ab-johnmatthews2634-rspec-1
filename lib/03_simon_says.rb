def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(string, times = 2)
  word_array = []
  until word_array.length == times
    word_array << string
  end
  word_array.join(" ")
end

def start_of_word(string, number_of_letters)
  string[0...number_of_letters]
end

def first_word(string)
  string.split[0]
end

def titleize(string)
  array_of_words = string.split
  array_of_words.each do |word|
    if word == "the" || word == "and" || word == "over"
      next
    else
      word.capitalize!
    end
  end
  array_of_words[0].capitalize!
  array_of_words.join(" ")
end
